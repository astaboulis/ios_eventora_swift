//
//  ModelUID.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 23/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import Foundation
import RealmSwift

class ModelUID : Object{
   
    @objc private(set) dynamic var id = 0
    
    @objc private(set) dynamic var uid = ""

    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(id : Int, uid: String) {
        self.init()
        self.id = id
        self.uid = uid
    }
}
