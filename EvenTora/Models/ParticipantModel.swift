//
//  ParticipantModel.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 18/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import Foundation

class ParticipantModel: NSObject {
    
    @objc private(set) dynamic var id = 0
    @objc private(set) dynamic var fullname = ""
    @objc private(set) dynamic var company = ""
    @objc private(set) dynamic var position = ""
    @objc private(set) dynamic var photo = ""

    
    convenience init(id : Int, fullname: String, company : String, position:String,photo:String) {
        self.init()
        self.id = id
        self.fullname = fullname
        self.company = company
        self.position = position
        self.photo=photo
    }
}
