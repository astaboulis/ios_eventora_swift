//
//  StartConversationViewController.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 13/2/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import UIKit

class StartConversationViewController: UIViewController {

    @IBAction func buttonConversation(_ sender: Any) {
        view.removeFromSuperview()
        let agendaVC = self.storyboard?.instantiateViewController(withIdentifier: "Agenda") as! Agenda
        addChild(agendaVC)
        self.view.addSubview(agendaVC.view)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
