//
//  ListQuestions.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 4/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit
import SQLite
import JGProgressHUD
class ListQuestions: UITableViewController,UITextViewDelegate {
    let requests=Requests()
    var clickCounter=Int()
    var getUID: [String] = []
    var setQuestionId=Int()
    var categoryID=Int()
    var getID=Int()
    let defaults=UserDefaults.standard
    var images: [UIImage] = []
    var setlikes:[String]=[]
    var setliked:[Bool]=[]
    var getQuestionId=Int()

    var setLiked=String()
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet weak var txtQuestion: UITextView!
    
    @IBAction func btnSaveRecord(_ sender: Any) {
        OperationQueue.main.addOperation {
            
                self.view.removeFromSuperview()

                let questionsVC = self.storyboard?.instantiateViewController(withIdentifier: "ListQuestions") as! ListQuestions
                self.addChild(questionsVC)
                self.view.addSubview(questionsVC.view)
        }
    }
    func refreshView(){
        
        let questionsVC = self.storyboard?.instantiateViewController(withIdentifier: "ListQuestions") as! ListQuestions
        self.addChild(questionsVC)
        self.view.addSubview(questionsVC.view)
    }
  
    @IBAction func buttonCloseForm(_ sender: Any) {
      self.dismiss(animated: true, completion:nil)
    }
    
    @IBAction func btnSend(_ sender: Any) {
        NetworkManager.isReachable { networkManagerInstance in
            let alertController = UIAlertController(title: "Ενημερωτικό Μήνυμα", message: "Η ερώτηση καταχωρήθηκε!!", preferredStyle: .alert)
            let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                let getid = self.defaults.integer(forKey:"id")
                self.likes.insert("0", at: 0)
                var requestUid=String()
                do{
                    let dbArray:String = Bundle.main.path(forResource: "dbparticipant", ofType: "db")!
                    let db = try Connection(dbArray)
                    let setuid = Table("setuid")
                    let uid = Expression<String?>("uid")
                    for uidString in try db.prepare(setuid) {
                        requestUid=uidString[uid]!
                    }
                }
                catch{
                    
                }
                self.requests.addQuestion(uid: requestUid, categoryID:getid,question:self.txtQuestion.text)
                self.questions.insert(self.txtQuestion.text, at:0)
                self.tableView.reloadData()
                self.txtQuestion.text=""
                alertController.dismiss(animated: true, completion:nil)
            }
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
            self.view.makeToast("Internet connection is OK", duration: 2.5, position: .center)
            self.txtQuestion.resignFirstResponder()

        }
        
        NetworkManager.isUnreachable { networkManagerInstance in
            self.view.makeToast("Internet connection is Failed", duration: 2.5, position: .center)
        }
    }
    var array: [Any] = []

    var questions: [String] = []{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    var questionsID: [String] = []
    var questionUID: [String] = []
    var likes: [String] = []{
        didSet{
            self.tableView.reloadData()
        }
    }

    var dates: [String] = []{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    var liked: [String] = []
    
    var tempquestions: NSMutableArray = []
    
    var tempdate: NSMutableArray = []
    
    var templikes: NSMutableArray = []
    
    var tempid: NSMutableArray = []

    func ListTempQuestions(){
        do{
           
                let tQuestionID = self.defaults.integer(forKey:"id")
                let likesId = self.defaults.integer(forKey: "id")
                let dateid = self.defaults.integer(forKey:"id")
                let uid = self.defaults.string(forKey:"uid")
                let getid = self.defaults.integer(forKey:"id")
                var requestUid=String()
                do{
                    let dbArray:String = Bundle.main.path(forResource: "dbparticipant", ofType: "db")!
                    let db = try Connection(dbArray)
                    let setuid = Table("setuid")
                    let uid = Expression<String?>("uid")
                    for uidString in try db.prepare(setuid) {
                        requestUid=uidString[uid]!
                    }
                }
                catch{
                    
                }
        OperationQueue.main.addOperation {
            DispatchQueue.main.async {
                self.requests.getQuestions(uid: requestUid, categoryId:tQuestionID, result:{result in
                    for countQuestions in 0..<result.count {
                        self.tempquestions.add(result[countQuestions])
                        
                        self.defaults.set(self.tempquestions, forKey: "questions")
                        self.defaults.synchronize()
                    }
                }, errorHandler:{error in
                    DispatchQueue.main.async {
                        let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                        let action1=UIAlertAction(title:"OK", style:.default, handler:
                        { action in
                            self.dismiss(animated: true, completion: nil)
                        })
                        controller.addAction(action1)
                        self.present(controller, animated:true, completion:nil)
                    }
                })
            }
        }
            OperationQueue.main.addOperation {
                DispatchQueue.main.async {
                self.requests.getQuestionsDate(uid: requestUid, categoryId:tQuestionID, result:{result in
                    for countDates in 0..<result.count {
                        self.tempdate.add(result[countDates])
                        self.defaults.set(self.tempdate, forKey: "dates")
                        self.defaults.synchronize()
                    }
                }, errorHandler:{error in
                    DispatchQueue.main.async {
                        let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                        let action1=UIAlertAction(title:"OK", style:.default, handler:
                        { action in
                            self.dismiss(animated: true, completion: nil)
                        })
                        controller.addAction(action1)
                        self.present(controller, animated:true, completion:nil)
                    }
                })
            }
        }
            OperationQueue.main.addOperation {
                DispatchQueue.main.async {
                self.requests.getLikes(uid: requestUid, categoryId:tQuestionID, result:{result in
                    for countLikes in 0..<result.count {
                        self.templikes.add(result[countLikes])
                        self.defaults.set(self.templikes, forKey: "likesArray")
                        self.defaults.synchronize()
                    }
                }, errorHandler:{error in
                    DispatchQueue.main.async {
                        let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                        let action1=UIAlertAction(title:"OK", style:.default, handler:
                        { action in
                            self.dismiss(animated: true, completion: nil)
                        })
                        controller.addAction(action1)
                        self.present(controller, animated:true, completion:nil)
                    }
                })
            }
        }
            OperationQueue.main.addOperation {
                DispatchQueue.main.async {

                self.requests.getQuestionsID(uid: requestUid, categoryId:tQuestionID, result:{result in
                    for countTempId in 0..<result.count {
                        self.tempid.add(result[countTempId])
                        self.defaults.set(self.tempid, forKey: "questionsIDArray")
                        self.defaults.synchronize()
                    }
                }, errorHandler:{error in
                    DispatchQueue.main.async {
                        let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                        let action1=UIAlertAction(title:"OK", style:.default, handler:
                        { action in
                            self.dismiss(animated: true, completion: nil)
                        })
                        controller.addAction(action1)
                        self.present(controller, animated:true, completion:nil)
                    }
                })
            }
        }
           
      }
      catch{
       
      }
       
    }
    func ListQuestions(){
        ListTempQuestions()
            OperationQueue.main.addOperation {
                if let arrayGetQuestions = self.defaults.array(forKey: "questions"){
                    for countQuestions in 0..<arrayGetQuestions.count{
                        self.questions.append(arrayGetQuestions[countQuestions] as! String)
                        self.images.append(UIImage(named: "like")!)
                        self.setlikes.append("false")
                        self.setliked.append(false)
                    }
            }
        }
        OperationQueue.main.addOperation {
            if let arrayGetDates = self.defaults.array(forKey: "dates"){
                for countDates in 0..<arrayGetDates.count{
                    self.dates.append(arrayGetDates[countDates] as! String)
                }
            }
        }
         OperationQueue.main.addOperation {
                if let arrayGetLikes = self.defaults.array(forKey: "likesArray"){
                        for countLikes in 0..<arrayGetLikes.count{
                                self.likes.append(arrayGetLikes[countLikes] as! String)
                        }
                }
        }
        OperationQueue.main.addOperation {
            if let arrayGetId = self.defaults.array(forKey: "questionsIDArray"){
                for countID in 0..<arrayGetId.count{
                    self.questionsID.append(arrayGetId[countID] as! String)
                }
            }
        }
    }
    @objc func gotoSite(){
        self.dismiss(animated: true, completion:nil)

    }
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnReturn: UIButton!
    
    @IBOutlet var message: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
       
    
        tableView.register(UINib(nibName: "QuestionCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.txtQuestion.delegate=self
        self.txtQuestion.text = "Type your Question"
        self.txtQuestion.textColor = UIColor.lightGray
        self.txtQuestion.layer.borderWidth = 1.0
        self.txtQuestion.layer.borderColor=UIColor.black.cgColor
        tableView.estimatedRowHeight=150
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        hud.dismiss(afterDelay:2.0)
        ListQuestions()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        txtQuestion.resignFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        

       // if(FBSDKAccessToken.current() != nil){
     //       let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
     //       graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                
    //            if let data = result as? NSDictionary
   //             {
   //                debugPrint(data)
    //            }
    //        })
     //   }
     

        
    }
  
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
             return false
        }
        return true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtQuestion.textColor == UIColor.lightGray {
            txtQuestion.text = nil
            txtQuestion.textColor = UIColor.black
        }
    }
    // MARK: - Table view data source
  
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return questions.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 150.0
    }
   
  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell: QuestionCell! = tableView.dequeueReusableCell(withIdentifier:"cell", for: indexPath) as? QuestionCell
            if(indexPath.row<self.questions.count){
                cell.lblFullName.text=self.questions[indexPath.row]
                cell.lblFullName.numberOfLines=0
                cell.lblFullName.lineBreakMode = .byWordWrapping
                
            }
            if(indexPath.row<self.likes.count){
                cell.lblCountLikes.text=self.likes[indexPath.row]
                cell.lblCountLikes.numberOfLines=0
                cell.lblCountLikes.lineBreakMode = .byWordWrapping
            }
            if(indexPath.row<self.dates.count){
                cell.lblDate.text=self.dates[indexPath.row]
            }
            if(indexPath.row<self.images.count){
                cell.btnLike.setImage(self.images[indexPath.row], for:.normal)
            }
            if(indexPath.row < self.setlikes.count){
                if(self.setlikes[indexPath.row].contains("true")){
                    cell.accessoryType = .checkmark
                }
                else{
                    cell.accessoryType = .none
                }
            }
            if let array = self.defaults.array(forKey: "likes"){
                if(indexPath.row < array.count){
                    if((array[indexPath.row] as AnyObject).contains("true")){
                        cell.accessoryType = .checkmark
                        self.setLiked="true"
                        self.setlikes[indexPath.row]="true"
                }
                else{
                        cell.accessoryType = .none
                    }
                }
            }

                    cell.btnLike.tag = indexPath.row
                    cell.btnLike.addTarget(self, action: #selector(self.btnLikeClicked(sender:)),
                                           for:.touchUpInside)
                
                
            
           
            
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func getNewUID()->String{
        var requestUid=String()
        do{
            let dbArray:String = Bundle.main.path(forResource: "dbparticipant", ofType: "db")!
            let db = try Connection(dbArray)
            let setuid = Table("setuid")
            let uid = Expression<String?>("uid")
            for uidString in try db.prepare(setuid) {
                requestUid=uidString[uid]!
            }
        }
        catch{
            
        }
        return requestUid
    }


    @objc func btnLikeClicked(sender: UIButton) {
        guard let cell = sender.superview?.superview as? QuestionCell else{
            return
        }
        if(sender.tag < self.questionsID.count){
            getQuestionId=Int(self.questionsID[sender.tag])!
        }
            if(self.setlikes[sender.tag].contains("false")){
                if (self.clickCounter==0){
                    self.clickCounter = Int(self.likes[sender.tag])!
                    self.clickCounter = self.clickCounter+1
                    self.likes[sender.tag]=String(self.clickCounter)
                    self.setliked[sender.tag]=true
                    self.setlikes[sender.tag]="true"
                    self.categoryID = self.defaults.integer(forKey:"id")
                    let uid = getNewUID()

                    self.requests.setLike(uid: uid, categoryID: self.categoryID, questionID:getQuestionId,liked:true)
                    cell.accessoryType = .checkmark
                    self.defaults.set(self.setlikes, forKey: "likes")
                    self.defaults.synchronize()
                    
                }
                else{
                    self.clickCounter = Int(self.likes[sender.tag])!
                    self.clickCounter = self.clickCounter+1
                    self.likes[sender.tag]=String(self.clickCounter)
                    self.setliked[sender.tag]=true
                    self.setlikes[sender.tag]="true"
                    self.categoryID = self.defaults.integer(forKey:"id")
                    let uid = self.defaults.string(forKey:"uid")
                    self.requests.setLike(uid: uid!, categoryID: self.categoryID, questionID:getQuestionId,liked:true)
                    cell.accessoryType = .checkmark
                    self.defaults.set(self.setlikes, forKey: "likes")
                    self.defaults.synchronize()
                }
                  
            }
            else{
                if (self.clickCounter==0){
                    self.clickCounter = Int(self.likes[sender.tag])!
                    self.clickCounter = self.clickCounter+1
                    self.likes[sender.tag]=String(self.clickCounter)
                    self.setliked[sender.tag]=true
                    self.setlikes[sender.tag]="true"
                    self.categoryID = self.defaults.integer(forKey:"id")
                    let uid =  self.defaults.string(forKey:"uid")

                    self.requests.setLike(uid: uid!, categoryID: self.categoryID, questionID:getQuestionId,liked:true)
                    cell.accessoryType = .checkmark
                    self.defaults.set(self.setlikes, forKey: "likes")
                    self.defaults.synchronize()
                    
                }
                else{
                    self.clickCounter = Int(self.likes[sender.tag])!
                    self.clickCounter = self.clickCounter-1
                    self.likes[sender.tag]=String(self.clickCounter)
                    self.setliked[sender.tag]=false
                    self.setlikes[sender.tag]="false"
                    self.categoryID = self.defaults.integer(forKey:"id")
                    let uid = self.defaults.string(forKey:"uid")
                    self.requests.setLike(uid: uid!, categoryID: self.categoryID, questionID:getQuestionId,liked:false)
                    cell.accessoryType = .none
                    self.defaults.set(self.setlikes, forKey: "likes")
                    self.defaults.synchronize()
                }

            }
            getID=sender.tag
        
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

