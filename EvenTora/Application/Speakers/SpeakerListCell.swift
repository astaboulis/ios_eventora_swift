//
//  SpeakerListCell.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 6/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import UIKit
import SDWebImage
import SQLite
class SpeakerListCell: UITableViewCell {

    @IBOutlet weak var btnTalk: UIButton!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }
 
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
