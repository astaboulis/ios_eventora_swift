//
//  Speakers.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 6/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import UIKit
//import FBSDKCoreKit
import SVProgressHUD
import SDWebImage
import RealmSwift
import RealmManager
import SQLite
import JGProgressHUD
import Kingfisher
class Speakers: UITableViewController {
    
    
    var getincrementID=Int()
    
    var getid=Int()
    
    let defaults=UserDefaults.standard

    let requests=Requests()

    @IBOutlet weak var txtSearch: UITextView!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet var btnReturnToMain: UIButton!
 
    var counter=Int()
    
    
  
    
    
    @IBAction func ReturnMain(_ sender: Any) {
        view.removeFromSuperview()
        let agendaVC = self.storyboard?.instantiateViewController(withIdentifier: "Agenda") as! Agenda
        addChild(agendaVC)
        self.view.addSubview(agendaVC.view)
    }
    
    @IBAction func btnClose(_ sender: Any) {
        txtSearch.text=""
    }
    @IBAction func btnSearch(_ sender: Any) {
        let resultPredicate = NSPredicate(format: "SELF contains[c] %@", txtSearch.text)

        arrayFullName = arrayFullName.filter { resultPredicate.evaluate(with: $0) }

        self.tableView!.reloadData()
        
    }

    var arrayFullName: [String] = []
    var arrayPhoto: [String]  = []
    var arrayPosition:[String] = []
    var getUID:NSMutableArray = []
    var questionUID:NSMutableArray = []
    var arrayCompany: [String] = []
    var arrayAllowChat: [String] = []

    func ListParticipant(){
 
            if let array = defaults.array(forKey: "participants"){
                    for countParticipants in 0..<array.count{
                        arrayFullName.append(array[countParticipants] as! String)
                    }
            }
            if let arrayGetCompany = defaults.array(forKey: "company"){
                for countCompany in 0..<arrayGetCompany.count{
                    
                    arrayCompany.append(arrayGetCompany[countCompany] as! String)
                }
            }
            if let arrayGetPosition = defaults.array(forKey: "position"){
                for countPosition in 0..<arrayGetPosition.count{
                    arrayPosition.append(arrayGetPosition[countPosition] as! String)
                }
            }
        if let arrayGetPhoto = defaults.array(forKey: "photo"){
            for countPhoto in 0..<arrayGetPhoto.count{
                arrayPhoto.append(arrayGetPhoto[countPhoto] as! String)
            }
        }
            if let arrayGetChat = defaults.array(forKey: "allowchat"){
                for countChat in 0..<arrayGetChat.count{
                    arrayAllowChat.append(arrayGetChat[countChat] as! String)
                }
            }
          
    }
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView!.register(UINib(nibName: "SpeakerListCell", bundle: nil), forCellReuseIdentifier: "cell")
      
        self.tableView!.estimatedRowHeight=310
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        NetworkManager.isReachable { networkManagerInstance in
            
            self.txtSearch.layer.borderWidth=1
            self.txtSearch.layer.borderColor=UIColor.black.cgColor
            let hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            hud.dismiss(afterDelay:5.0)
            
            self.ListParticipant()
            self.view.makeToast("Internet connection is OK", duration: 2.5, position: .center)
        }
        
        NetworkManager.isUnreachable { networkManagerInstance in
            self.txtSearch.layer.borderWidth=1
            self.txtSearch.layer.borderColor=UIColor.black.cgColor
            
            self.view.makeToast("Internet connection is Failed", duration: 2.5, position: .center)
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 310.0
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrayFullName.count
    }
  
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SpeakerListCell! = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? SpeakerListCell
      
        if(indexPath.row<(arrayFullName.count)){
            cell.lblFullName.text=arrayFullName[indexPath.row]
            debugPrint(arrayFullName[indexPath.row])
        }
       
        if(indexPath.row<(arrayCompany.count)){
            cell.lblCompany.text=arrayCompany[indexPath.row]
        }
        if(indexPath.row<(arrayPosition.count)){
            cell.lblTitle.text=arrayPosition[indexPath.row]
        }
        if(indexPath.row<(arrayPhoto.count)){
            DispatchQueue.main.async{
                cell.img.sd_setImage(with: URL(string:self.arrayPhoto[indexPath.row]), placeholderImage: UIImage(named: "logo.png"))
            }
        }
        if(indexPath.row<(arrayAllowChat.count)){
            if(arrayAllowChat[indexPath.row].contains("false")){
                    cell.btnTalk.isHidden=true
            }
            else{
                    cell.btnTalk.isHidden=false
            }
        }
        DispatchQueue.main.async {
            cell.btnTalk.tag = indexPath.row
            cell.btnTalk.addTarget(self, action: #selector(self.buttonConversation(sender:)),
                                   for:.touchUpInside)
        }
      
        return cell
    }
    @objc func buttonConversation(sender: UIButton) {
        let agendaVC = self.storyboard?.instantiateViewController(withIdentifier: "StartConversation") as! StartConversationViewController
        addChild(agendaVC)
        self.view.addSubview(agendaVC.view)

    }
  
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
