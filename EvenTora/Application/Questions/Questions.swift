//
//  Questions.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 4/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import UIKit

class Questions: UITableViewController,UIGestureRecognizerDelegate {
    let requests=Requests()
    var getcategoriesid=Int()
    var categoriesID=Int()
    let defaults=UserDefaults.standard
    var categoriesId: [Int] = []
    var categories: [String] = []
    var uid=String()
    var setCategories: [String] = []
    var setCategoriesId: [Int] = []
    var questionsID: [String] = []
   
    func ListCategories(){
        if let array = defaults.array(forKey: "categories"){
            for countArray in 0..<array.count{
                setCategories.append(array[countArray] as! String)
            }
        }
    }
    func ListCategoriesid(){
        if let array = defaults.array(forKey: "categoriesId"){
            for countArray in 0..<array.count{
                setCategoriesId.append(array[countArray] as! Int)
            }

        }
        
    }
    func ListQuestionsid(categoryID:Int){
        let uid=defaults.string(forKey: "uid")
        if let array = defaults.array(forKey: "questionsID"){
            debugPrint(array.count)

        }
    }
    
    
    @objc func gotoSite(){
        self.dismiss(animated: true, completion:nil)

        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate=self
        tableView.dataSource=self
       
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        self.ListCategories()
        self.ListCategoriesid()
        
        
        
        NetworkManager.isUnreachable { networkManagerInstance in
            self.view.makeToast("Internet connection is Failed", duration: 1.5, position: .center)
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    // MARK: - Table view data source
   
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.setCategories.count
    }
   
   
 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text=setCategories[indexPath.row]
        cell.tag = indexPath.row
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapEdit(sender:)))
        tableView.addGestureRecognizer(tapGesture)
        cell.isUserInteractionEnabled=true
        return cell
    }
    @objc func tapEdit(sender: UITapGestureRecognizer) {
        // change data model blah-blah
        self.defaults.set(self.setCategoriesId[sender.view!.tag], forKey: "id")
        self.defaults.set(self.uid, forKey: "uid")
        self.defaults.synchronize()
        let questionsVC = self.storyboard?.instantiateViewController(withIdentifier: "ListQuestions") as! ListQuestions
        self.addChild(questionsVC)
        self.view.addSubview(questionsVC.view)
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
