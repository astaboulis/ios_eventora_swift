//
//  MainTab.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 5/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKCoreKit
import SQLite
class MainTab: UITabBarController,UITabBarControllerDelegate {
    let requests=Requests()
    var uid:[String]=[]
    let defaults=UserDefaults.standard
    
    var categories: [String] = []
    var categoriesId: [Int] = []
    var questionsID: [String] = []
    var getUID: [String] = []
    var tempparticipants: NSMutableArray = []
    var tempcompany: NSMutableArray = []
    var tempposition: NSMutableArray = []
    var tempallowchat: NSMutableArray = []
    var tempphoto: NSMutableArray = []
    
    func ListParticipants(){
        OperationQueue.main.addOperation {
            self.requests.getUID(result: { (result) in
                for countUID in (0..<result.count) {
                    self.getUID.append(result[countUID])
                }
                self.requests.getParticipantsFullName(uid: self.getUID[0], result: { (result) in
                    for countParticipants in (0..<result.count) {
                        self.tempparticipants.add(result[countParticipants])
                    }
                    self.defaults.set(self.tempparticipants, forKey: "participants")
                    self.defaults.synchronize()
                }, errorHandler: { (error) in
                    let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                    let action1=UIAlertAction(title:"OK", style:.default, handler:
                    { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    controller.addAction(action1)
                    
                    self.present(controller, animated:true, completion:nil)
                })
                self.requests.getParticipantsCompany(uid: self.getUID[0], result: { (result) in
                    for countCompany in (0..<result.count) {
                        self.tempcompany.add(result[countCompany])
                    }
                    self.defaults.set(self.tempcompany, forKey: "company")
                    self.defaults.synchronize()
                }, errorHandler: { (error) in
                    let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                    let action1=UIAlertAction(title:"OK", style:.default, handler:
                    { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    controller.addAction(action1)
                    
                    self.present(controller, animated:true, completion:nil)
                })
                self.requests.getParticipantsPosition(uid: self.getUID[0], result: { (result) in
                    for countPosition in (0..<result.count) {
                        self.tempposition.add(result[countPosition])
                    }
                    self.defaults.set(self.tempposition, forKey: "position")
                    self.defaults.synchronize()
                }, errorHandler: { (error) in
                    let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                    let action1=UIAlertAction(title:"OK", style:.default, handler:
                    { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    controller.addAction(action1)
                    
                    self.present(controller, animated:true, completion:nil)
                })
                self.requests.getParticipantsPhoto(uid: self.getUID[0], result: { (result) in
                    for countPhoto in (0..<result.count) {
                        self.tempphoto.add(result[countPhoto])
                    }
                    self.defaults.set(self.tempphoto, forKey: "photo")
                    self.defaults.synchronize()
                }, errorHandler: { (error) in
                    let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                    let action1=UIAlertAction(title:"OK", style:.default, handler:
                    { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    controller.addAction(action1)
                    
                    self.present(controller, animated:true, completion:nil)
                })
                self.requests.getParticipantsAllowChat(uid: self.getUID[0], result: { (result) in
                    for countAllowChat in (0..<result.count) {
                        self.tempallowchat.add(result[countAllowChat])
                    }
                    self.defaults.set(self.tempallowchat, forKey: "allowchat")
                    self.defaults.synchronize()
                }, errorHandler: { (error) in
                    let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                    let action1=UIAlertAction(title:"OK", style:.default, handler:
                    { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    controller.addAction(action1)
                    
                    self.present(controller, animated:true, completion:nil)
                })
            }, errorHandler: { (error) in
                let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                let action1=UIAlertAction(title:"OK", style:.default, handler:
                { action in
                    self.dismiss(animated: true, completion: nil)
                })
                controller.addAction(action1)
                
                self.present(controller, animated:true, completion:nil)
            })
           
    }
}
        
 
    func ListCategories(){
        requests.getQuestionsCategories(result: {
            result in
            for countCategories in 0..<result.count {
                self.categories.append(result[countCategories])
            }
            self.defaults.set(self.categories, forKey: "categories")
            self.defaults.synchronize()
        },errorHandler:{error in
            DispatchQueue.main.async {
                let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                let action1=UIAlertAction(title:"OK", style:.default, handler:
                { action in
                   self.dismiss(animated: true, completion: nil)
                })
                controller.addAction(action1)
                
                self.present(controller, animated:true, completion:nil)
            }

          
        })
    }
    func ListCategoriesid(){
        requests.getQuestionsCategoriesId(result: {
            result in
            for countCategoriesId in 0..<result.count {
                self.categoriesId.append(result[countCategoriesId])
            }
            self.defaults.set(self.categoriesId, forKey: "categoriesId")
            self.defaults.synchronize()
        },errorHandler:{error in
            debugPrint(error)
            DispatchQueue.main.async {
                let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                let action1=UIAlertAction(title:"OK", style:.default, handler:
                { action in
                  self.dismiss(animated: true, completion: nil)
                })
                controller.addAction(action1)
                
                self.present(controller, animated:true, completion:nil)
                self.present(controller, animated:true, completion:nil)
            }
         
        })
    }
    func ListQuestionsid(categoryID:Int){
        var requestUid=String()
        do{
            let dbArray:String = Bundle.main.path(forResource: "dbparticipant", ofType: "db")!
            let db = try Connection(dbArray)
            let setuid = Table("setuid")
            let uid = Expression<String?>("uid")
            for uidString in try db.prepare(setuid) {
                requestUid=uidString[uid]!
            }
            requests.getQuestionsID(uid: requestUid,categoryId: categoryID, result: {
                result in
                for countQuestionsId in 0..<result.count {
                    self.questionsID.append(result[countQuestionsId])
                }
                self.defaults.set(self.questionsID, forKey: "questionsID")
                self.defaults.synchronize()
            },errorHandler:{error in
                debugPrint(error)
                DispatchQueue.main.async {
                    let controller = UIAlertController(title:"Ενημερωτικό Μήνυμα", message:error, preferredStyle: .alert)
                    let action1=UIAlertAction(title:"OK", style:.default, handler:
                    { action in
                        // self.dismiss(animated: true, completion: nil)
                    })
                    controller.addAction(action1)
                    
                    self.present(controller, animated:true, completion:nil)
                }
                
                
            })
        }
        catch{
            
        }
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
     
        NetworkManager.isReachable { networkManagerInstance in
            var language = Locale.current.languageCode
            if(!language!.contains("en")){
                let tabBar1 = self.tabBar.items![0]
                let tabBar2 = self.tabBar.items![1]
                let tabBar3 = self.tabBar.items![2]
                let tabBar4 = self.tabBar.items![3]
                tabBar1.title = "Πρόγραμμα Εκδήλωσης"
                tabBar2.title="Δημοσκοπήσεις"
                tabBar3.title="Περισσότερα"
                tabBar4.title="Ερωτήσεις"
            }
            OperationQueue.main.addOperation {
                self.ListParticipants()

            }
            OperationQueue.main.addOperation {
                self.ListCategories()
            }
            OperationQueue.main.addOperation {
                self.ListCategoriesid()

            }
         
            
            
            self.view.makeToast("Internet connection is OK", duration: 2.5, position: .center)
        }
        
        NetworkManager.isUnreachable { networkManagerInstance in
           self.view.makeToast("Internet connection is Failed", duration: 2.5, position: .center)
        }
       
        // Do any additional setup after loading the view.
    }
 
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
