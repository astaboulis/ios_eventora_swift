//
//  Requests.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 4/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SQLite
import Toast_Swift
import UIKit
class Requests{
    
    var API_PARTICIPANTS="http://api.eventora.com/testiosapp/participants/"
    
    var API_BASE_URL_QUESTIONS="http://api.eventora.com/testiosapp/questions/"

    var API_BASE_URL_QUESTIONS_CATEGORIES="http://api.eventora.com/questions/categories/testiosapp"
    
    var API_BASE_URL_ADD_QUESTION="http://api.eventora.com/testiosapp/questions/"

    var API_POST_LIKE_QUESTION="http://api.eventora.com/testiosapp/questions/"
    
    var API_GET_LIKE_QUESTION="http://api.eventora.com/testiosapp/questions/"
    
    var API_UID="http://api.eventora.com/testiosapp/home/getUid"
    
    
    
    let defaults=UserDefaults.standard
    
    func getUID(result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
        //let params:NSMutableDictionary? = ["eventSlug":"testiosapp"]

        let ulr =  NSURL(string:API_UID)
        
        var request = URLRequest(url: ulr! as URL)
        

        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")

        // let data = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        //let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        //request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        let manager = Alamofire.SessionManager.default
        manager.request(request as! URLRequestConvertible)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    var questionUID: [String] = []
                    questionUID.append(json["uid"].stringValue)

                    DispatchQueue.main.async {
                        do{
                            let dbArray:String = Bundle.main.path(forResource: "dbparticipant", ofType: "db")!
                            let db = try Connection(dbArray)
                            let setuid = Table("setuid")
                            let uid = Expression<String?>("uid")
                            let countRecords=try db.scalar(setuid.count)
                            if(countRecords>0){
                                try db.run(setuid.delete())
                            }
                            else{
                                let insert = setuid.insert(uid <- json["uid"].stringValue)
                                let rowid = try db.run(insert)
                            }
                        }
                        catch{
                            
                        }
                    }
                   
                    result(questionUID)
                case .failure(let error):
                    errorHandler(error.localizedDescription)
                }
        }
    }
    
    func getParticipantsFullName(uid:String,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
        let ulr =  NSURL(string:API_PARTICIPANTS+uid)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")

        request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")

        OperationQueue.main.addOperation {
            DispatchQueue.main.async {
             Alamofire.request(request as! URLRequestConvertible)
                
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let json = JSON(response.result.value!)
                        var lastnames = [String]()
                        for countLastNames in 0..<1000{
                            
                            lastnames.append(json["participantsList"][countLastNames]["lastName"].stringValue+" "+json["participantsList"][countLastNames]["firstName"].stringValue)
                            do{
                                let dbArray:String = Bundle.main.path(forResource: "dbparticipant", ofType: "db")!
                                let db = try Connection(dbArray)
                                let participants = Table("participants")
                                let countRecords=try db.scalar(participants.count)
                                if(countRecords>0){
                                    try db.run(participants.delete())
                                }
                                else{
                                    let id = Expression<Int64>("id")
                                    let name = Expression<String?>("fullname")
                                    let company = Expression<String>("company")
                                    let position = Expression<String>("position")
                                    let photo = Expression<String>("photo")
                                    let insert = participants.insert(id <- Int64(countLastNames),name <- json["participantsList"][countLastNames]["lastName"].stringValue+" "+json["participantsList"][countLastNames]["firstName"].stringValue , company <- json["participantsList"][countLastNames]["company"].stringValue,position <- json["participantsList"][countLastNames]["position"].stringValue , photo <- json["participantsList"][countLastNames]["photo"].stringValue)
                                    let rowid = try db.run(insert)
                                }
                            }
                            catch{
                                
                            }
                            
                            
                            
                            
                            
                            result(lastnames)
                        }
                    case .failure(let error):
                        errorHandler(error.localizedDescription)
                        
                    }
                }
            }
        }
        
}
    func getParticipantsCompany(uid:String,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
        let ulr =  NSURL(string:API_PARTICIPANTS+uid)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")
        OperationQueue.main.addOperation {
            DispatchQueue.main.async{
                Alamofire.request(request as! URLRequestConvertible)
                    
                    .responseJSON { response in
                        switch response.result {
                        case .success:
                            let json = JSON(response.result.value!)
                            var company = [String]()
                            for countCompany in 0..<1000{
                                company.append(json["participantsList"][countCompany]["company"].stringValue)
                            }
                            result(company)
                            
                            
                            
                        case .failure(let error):
                            errorHandler(error.localizedDescription)
                        }
                }
            }
        }
    }
    func getParticipantsPosition(uid:String,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
        let ulr =  NSURL(string:API_PARTICIPANTS+uid)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")
        
    OperationQueue.main.addOperation {
        DispatchQueue.main.async{
            Alamofire.request(request as! URLRequestConvertible)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let json = JSON(response.result.value!)
                        var position = [String]()
                        for countPosition in 0..<1000{
                            position.append(json["participantsList"][countPosition]["position"].stringValue)
                        }
                        result(position)
                        
                        
                        
                    case .failure(let error):
                        errorHandler(error.localizedDescription)
                    }
                }
            }
        }
    }
    func getParticipantsAllowChat(uid:String,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
        let ulr =  NSURL(string:API_PARTICIPANTS+uid)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"

        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")
    
        
    OperationQueue.main.addOperation {
        DispatchQueue.main.async{
            Alamofire.request(request as! URLRequestConvertible)
                
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let json = JSON(response.result.value!)
                        var position = [String]()
                        for countPosition in 0..<1000{
                            position.append(json["participantsList"][countPosition]["allowChat"].stringValue)
                            
                        }
                        result(position)
                    case .failure(let error):
                        errorHandler(error.localizedDescription)
                    }
                }
            }
        }
   }
    func getParticipantsPhoto(uid:String,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
        let ulr =  NSURL(string:API_PARTICIPANTS+uid)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        

        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")
        OperationQueue.main.addOperation {
            DispatchQueue.main.async{
                Alamofire.request(request as! URLRequestConvertible)
                    
                    .responseJSON { response in
                        switch response.result {
                        case .success:
                            let json = JSON(response.result.value!)
                            var photo = [String]()
                            for countPhoto in 0..<1000{
                                photo.append(json["participantsList"][countPhoto]["photo"].stringValue)
                                
                            }
                            result(photo)
                            
                        case .failure(let error):
                            errorHandler(error.localizedDescription)
                            
                        }
                }
            }
        }

}
    func setLike(uid:String,categoryID:Int,questionID:Int,liked:Bool){
            debugPrint("liked:",liked)
            let params:NSMutableDictionary? = ["liked":liked,"uid":uid]
            let ulr =  NSURL(string:self.API_POST_LIKE_QUESTION+"/"+String(categoryID)+"/likeQuestion/"+String(questionID))
            var request = URLRequest(url: ulr! as URL)
            request.httpMethod = "POST"

            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
            request.setValue("3495", forHTTPHeaderField:"Content-Length")

            request.setValue("application/json", forHTTPHeaderField: "Accept")
        
            request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")
        
            let data = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            
            request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
            
            Alamofire.request(request as! URLRequestConvertible)
                .validate()
                .responseString { response in
                    debugPrint(response.result.value)
            }
        
    }
    func addQuestion(uid:String,categoryID:Int,question:String){
            let params:NSMutableDictionary? = ["eventSlug":"testiosapp","categoryid":categoryID,"question":question,"uid":uid]

            let ulr =  NSURL(string:self.API_BASE_URL_ADD_QUESTION+"/"+String(categoryID)+"/addQuestion")
            var request = URLRequest(url: ulr! as URL)
            request.httpMethod = "POST"

            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")
            let data = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
        
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
            request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
            Alamofire.request(request as! URLRequestConvertible)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let json = JSON(response.result.value!)
                        debugPrint(json)
                    case .failure(let error):
                        debugPrint(error.localizedDescription)
                }
        }
    }
    func getQuestionUID(uid:String,categoryId:Int,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
        //let params:NSMutableDictionary? = ["eventSlug":"testiosapp","categoryId":categoryId]

        let ulr =  NSURL(string:self.API_BASE_URL_QUESTIONS+String(categoryId)+"/questionsList/" + uid)
        var request = URLRequest(url: ulr! as URL)
        
        request.httpMethod = "GET"
        
        //request.timeoutInterval=60

        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
      
        request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")

        //let data = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        //let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        //request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
      
            Alamofire.request(request as! URLRequestConvertible)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let json = JSON(response.result.value!)
                        var uid = [String]()
                        for countQuestions in 0..<json["questionCategory"]["questions"].count {
                            uid.append(json["questionCategory"]["questions"][countQuestions]["uid"].stringValue)
                        }
                        result(uid)
                        
                    case .failure(let error):
                        errorHandler(error.localizedDescription)
                    }
            }
         
    }
    func getLikes(uid:String,categoryId:Int,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
           // let params:NSMutableDictionary? = ["eventSlug":"testiosapp","categoryId":categoryId]

            let ulr =  NSURL(string:self.API_BASE_URL_QUESTIONS+String(categoryId)+"/questionsList/" + uid)
            var request = URLRequest(url: ulr! as URL)

            request.httpMethod = "GET"
        
        
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
            request.setValue("application/json", forHTTPHeaderField: "Accept")
        
            request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")

            //let data = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
        
           // let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
           // request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
       
                Alamofire.request(request as! URLRequestConvertible)
                    
                    .responseJSON { response in
                        switch response.result {
                        case .success:
                            let json = JSON(response.result.value!)
                            var likes = [String]()
                            for countQuestions in 0..<json["questionCategory"]["questions"].count {
                                likes.append(json["questionCategory"]["questions"][countQuestions]["likes"].stringValue)
                            }
                            result(likes)
                            
                        case .failure(let error):
                            errorHandler(error.localizedDescription)
                        }
                }
        
          
    
        
        
        
    }
    func getQuestions(uid:String,categoryId:Int,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
            debugPrint("uid=",uid)
           // let params:NSMutableDictionary? = ["eventSlug":"testiosapp","categoryId":categoryId]
            let ulr =  NSURL(string:self.API_BASE_URL_QUESTIONS+String(categoryId)+"/questionsList/"+uid)
            var request = URLRequest(url: ulr! as URL)
            request.httpMethod = "GET"


            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")
            //let data = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
        
            //let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
           // request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
                    Alamofire.request(request as! URLRequestConvertible)
                        
                        .responseJSON { response in
                            switch response.result {
                            case .success:
                                let json = JSON(response.result.value!)
                                var questions = [String]()
                                for countQuestions in 0..<json["questionCategory"]["questions"].count {
                                    questions.append(json["questionCategory"]["questions"][countQuestions]["question"].stringValue)
                                }
                                
                                result(questions)
                            case .failure(let error):
                                errorHandler(error.localizedDescription)
                            }
                    }
    }

    func getQuestionsLikedFlag(uid:String,categoryId:Int,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
       
       // let params:NSMutableDictionary? = ["eventSlug":"testiosapp","categoryId":categoryId]

        
        let ulr =  NSURL(string:self.API_BASE_URL_QUESTIONS+String(categoryId)+"/questionsList/"+uid)
        var request = URLRequest(url: ulr! as URL)

        request.httpMethod = "GET"

        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")

        //let data = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        //let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
       // request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
      
            Alamofire.request(request as! URLRequestConvertible)
                
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let json = JSON(response.result.value!)
                        var liked = [String]()
                        for countLiked in 0..<json["questionCategory"]["questions"].count {
                            liked.append(json["questionCategory"]["questions"][countLiked]["liked"].stringValue)
                        }
                        result(liked)
                        
                    case .failure(let error):
                        errorHandler(error.localizedDescription)
                    }
                }
        
}

    func getQuestionsCategories(result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
        
        let ulr =  NSURL(string:self.API_BASE_URL_QUESTIONS_CATEGORIES)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"

        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")
        OperationQueue.main.addOperation {
            Alamofire.request(request as! URLRequestConvertible)
                
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        var json = JSON(response.result.value!)
                        var arrayCategories = [String]()
                        for countCategories in 0..<json["questionCategories"].count {
                            arrayCategories.append(json["questionCategories"][countCategories]["categoryName"].stringValue)
                        }
                        result(arrayCategories)
                        
                    case .failure(let error):
                        errorHandler(error.localizedDescription)
                    }
            }
        }
        
       
}
func getQuestionsCategoriesId(result: @escaping ([Int]) -> (),errorHandler: @escaping(String)->()){
        let ulr =  NSURL(string:self.API_BASE_URL_QUESTIONS_CATEGORIES)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")
    OperationQueue.main.addOperation {
        Alamofire.request(request as! URLRequestConvertible)
            
            .responseJSON { response in
                switch response.result {
                case .success:
                    var json = JSON(response.result.value!)
                    var arrayCategories = [Int]()
                    for countCategories in 0..<json["questionCategories"].count {
                        arrayCategories.append(json["questionCategories"][countCategories]["categoryId"].intValue)
                        
                    }
                    result(arrayCategories)
                    
                case .failure(let error):
                    errorHandler(error.localizedDescription)
                }
                
        }
    }
   
}
    func getQuestionsDate(uid:String,categoryId:Int,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
           // let params:NSMutableDictionary? = ["eventSlug":"testiosapp","categoryId":categoryId]

            let ulr =  NSURL(string:self.API_BASE_URL_QUESTIONS+String(categoryId)+"/questionsList/"+uid)
            var request = URLRequest(url: ulr! as URL)
            request.httpMethod = "GET"

            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")

            //let data = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
        
            //let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
            //request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
           Alamofire.request(request as! URLRequestConvertible)
                
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let json = JSON(response.result.value!)
                        var questionsDate = [String]()
                        for countQuestions in 0..<json["questionCategory"]["questions"].count {
                            
                            let timeResult = (json["questionCategory"]["questions"][countQuestions]["dateCreated"].doubleValue)
                            
                            let date = Date(timeIntervalSince1970: timeResult)
                            let dateFormatter = DateFormatter()
                            dateFormatter.timeStyle = DateFormatter.Style.medium
                            dateFormatter.dateStyle = DateFormatter.Style.medium
                            let localDate = dateFormatter.string(from: date)
                            questionsDate.append(localDate)
                        }
                        result(questionsDate)
                        
                    case .failure(let error):
                        errorHandler(error.localizedDescription)
                    }
                }
            
    }

    func getQuestionsID(uid:String,categoryId:Int,result: @escaping ([String]) -> (),errorHandler: @escaping(String)->()){
       //     let params:NSMutableDictionary? = ["eventSlug":"testiosapp","categoryId":categoryId]

            let ulr =  NSURL(string:self.API_BASE_URL_QUESTIONS+String(categoryId)+"/questionsList/"+uid)
            var request = URLRequest(url: ulr! as URL)
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw", forHTTPHeaderField: "apiKey")

            //let data = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
        
           // let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
           // request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
      
            Alamofire.request(request as! URLRequestConvertible)
                
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let json = JSON(response.result.value!)
                        var questionsID = [String]()
                        for countQuestionsID in 0..<json["questionCategory"]["questions"].count {
                            
                            questionsID.append(json["questionCategory"]["questions"][countQuestionsID]["questionId"].stringValue)
                          
                        }
                        result(questionsID)
                        
                    case .failure(let error):
                        errorHandler(error.localizedDescription)
                    }
            }
        }
}
