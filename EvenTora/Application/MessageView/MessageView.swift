//
//  MessageView.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 7/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import UIKit

class MessageView: UIViewController {

    @IBOutlet var txtMessage: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtMessage.layer.borderColor=UIColor.black.cgColor
        txtMessage.layer.borderWidth=1.0
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
