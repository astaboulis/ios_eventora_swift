//
//  StartScreenViewController.swift
//  EvenTora
//
//  Created by Angelos Staboulis on 5/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

import UIKit
import FBSDKCoreKit
class StartScreenViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var startImage: UIImageView!
    
    func createRoundedButton(){
        btnLogin.backgroundColor=UIColor.white
        btnLogin.layer.borderColor=UIColor.white.cgColor
        btnLogin.layer.cornerRadius=10
        btnLogin.layer.borderWidth=1.0
        btnLogin.clipsToBounds=true
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        startImage.frame=CGRect(x:0,y:0,width:UIScreen.main.bounds.width,height:UIScreen.main.bounds.height)
        createRoundedButton()
        txtPassword.isSecureTextEntry=true
        txtUserName.delegate=self
        txtPassword.delegate=self
       
       
        // Do any additional setup after loading the view.
    }
   
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
